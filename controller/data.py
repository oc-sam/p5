from itertools import starmap
from typing import List, Tuple

from api.deserialization import ToInstances
from database.connector import DbConnector
from models.entities.brand import Brand
from models.entities.category import Category
from models.entities.product import Product
from models.entities.productbrand import ProductBrand
from models.entities.productcategory import ProductCategory
from models.entities.productstore import ProductStore
from models.entities.productsubstitute import ProductSubstitute
from models.entities.store import Store
from settings.db import DB_NAME
from utils.controllers import CtrlTools
from utils.ressources.db.sub_query import SUB_Q


class DataController(CtrlTools):
    """
    DataController controller for data related tasks.

    Args:
        CtrlTools (class): controllers tools
    """
    dbconnect = DbConnector()

    def __init__(self, request_data=True) -> None:
        """
        __init__ request data and populate db if needed.

        Args:
            request_data (bool, optional): wether or not
            the data should be downloaded. Defaults to True.
        """
        if request_data:
            self.init_tables()
            self.get_data()
            self.populate_init_db()
            self.populate_db()

    def get_prods_from_cat(self, cat_id: int) -> List[tuple]:
        """
        get_prods_from_cat query search product data related to categories.

        Args:
            cat_id (int): category id.

        Returns:
            List[tuple]: Products informations.
        """
        prod_id_cat = ProductCategory.objects.filter(
            wanted='id_prod',
            field_name='id_cat',
            operation='=',
            value=cat_id
        )
        return Product.objects.filter(
            wanted='*',
            field_name='id',
            operation='IN',
            value=self.unpack_res(prod_id_cat)
        )

    def get_subs_from_prodsub(self) -> Tuple[List[tuple]]:
        """
        get_subs_from_prodsub query search a product data and its subsitute
        data.

        Returns:
            Tuple[List[tuple]]: product data with its substitute data.
        """
        id_prods, id_subs = [], []
        ids = ProductSubstitute.objects.get_all()
        for id_pair in ids:
            id_prods.append(id_pair[0])
            id_subs.append(id_pair[1])
        return (
            Product.objects.filter(
                wanted='*',
                field_name='id',
                operation='IN',
                value=tuple(id_prods)
            ),
            Product.objects.filter(
                wanted='*',
                field_name='id',
                operation='IN',
                value=tuple(id_subs)
            )
        )

    def init_tables(self):
        """
        init_tables creates db tables.
        """
        if not self.dbconnect.tables_created():
            self.dbconnect.create_tables()

    def get_sub(self, prod_id) -> tuple:
        """
        get_sub get a substitute product.

        Args:
            prod_id (int): substituted product id.

        Returns:
            tuple: Substitut infos.
        """
        sub = Product.objects.filter(custom_q=SUB_Q.format(prod_id, DB_NAME))
        if sub == []:
            return None
        else:
            return sub[0]

    def get_prod_related(self, prod_id):
        """
        get_prod_related query search a product related brand and store.

        Args:
            prod_id (int): product id.

        Returns:
            dict: brands and stores names.
        """
        brands_id = self.unpack_res(
            ProductBrand.objects.filter(
                wanted='id_brand',
                field_name='id_prod',
                operation='=',
                value=prod_id
            )
        )
        stores_id = self.unpack_res(
            ProductStore.objects.filter(
                wanted='id_store',
                field_name='id_prod',
                operation='=',
                value=prod_id
            )
        )
        brands = self.unpack_res(
            Brand.objects.filter(
                wanted='name',
                field_name='id',
                operation='IN',
                value=brands_id
            )
        )
        stores = self.unpack_res(
            Store.objects.filter(
                wanted='name',
                field_name='id',
                operation='IN',
                value=stores_id
            )
        )
        return {
            'brand': brands,
            'store': stores,
        }

    @staticmethod
    def get_all_cats():
        """
        get_all_cats get all categories.

        Returns:
            List[tuple]: all categories informations.
        """
        return Category.objects.get_all()

    def get_data(self):
        """
        get_data get data through API and set related instances as attributes.
        """
        self.init_instances = ToInstances().init_instances
        for model_name, list_instances in self.init_instances.items():
            setattr(self, model_name.lower(), list_instances)

    def populate_init_db(self):
        """
        populate_init_db store requested informations in db.
        """
        for table_name, instances_list in self.init_instances.items():
            if table_name == 'Product':
                Product.objects.save(instances_list)
            elif table_name == 'Category':
                Category.objects.save(instances_list)
            elif table_name == 'Store':
                Store.objects.save(instances_list)
            elif table_name == 'Brand':
                Brand.objects.save(instances_list)
            else:
                print('Error: cannot populate_init_db')

    def add_sub(self, prod_choice, sub_id):
        """
        add_sub add a new sub/prod pair to db.

        Args:
            prod_choice (int): product id.
            sub_id (int): substitute id.
        """
        ProductSubstitute.objects.save((prod_choice, sub_id))

    def populate_db(self):
        """
        populate_db store intermediaries models ids in db.
        """
        all_related_ids = self.stargazing()
        for list_name, ids_list in all_related_ids.items():
            if list_name == 'prod_cat_ids':
                ProductCategory.objects.save(ids_list)
            if list_name == 'prod_store_ids':
                ProductStore.objects.save(ids_list)
            if list_name == 'prod_brand_ids':
                ProductBrand.objects.save(ids_list)

    def stargazing(self):
        """
        Join each product id its related information id.

        Populate intermediary tables for each product and its related
        informations.
        Related information: stores, brands and categories
        Explanation:
        - get for all names.
        - query for already registered objects, using names.
        - query all product related data
            - get all related infos names
            - pair related objects by selecting related ids

        Returns:
            dict: all related ids.
        """

        product_names, category_names, brand_names, store_names = tuple(
            starmap(
                self.get_names,
                (
                    (self.product,),
                    (self.category,),
                    (self.brand,),
                    (self.store,)
                )
            )
        )

        saved_prods, saved_cats, saved_brands, saved_stores = tuple(
            starmap(
                self.q_saved,
                (
                    (Product, product_names),
                    (Category, category_names),
                    (Brand, brand_names),
                    (Store, store_names)
                )
            )
        )

        prod_cat_ids, prod_store_ids, prod_brand_ids = [], [], []

        for prod in saved_prods:
            for product in self.product:
                if product.name == prod[1]:
                    prod_id = prod[0]
                    related_cats, related_brands, related_stores = tuple(
                        starmap(
                            self.get_names,
                            (
                                (product.categories,),
                                (product.brands,),
                                (product.stores,)
                            )
                        )
                    )
                    prod_cat_ids += self.get_id_pair(
                        prod_id,
                        related_cats,
                        saved_cats
                    )
                    prod_store_ids += self.get_id_pair(
                        prod_id,
                        related_stores,
                        saved_stores
                    )
                    prod_brand_ids += self.get_id_pair(
                        prod_id,
                        related_brands,
                        saved_brands
                    )
        return dict(
            (
                ("prod_cat_ids", prod_cat_ids),
                ("prod_store_ids", prod_store_ids),
                ("prod_brand_ids", prod_brand_ids)
            )
        )

    @staticmethod
    def get_prod(prod_choice):
        """
        get_prod get a product information given its id.

        Args:
            prod_choice (int): product id.

        Returns:
            Union[None, List[tuple]]: product infos.
        """
        return Product.objects.filter(
            wanted='*',
            field_name='id',
            operation='=',
            value=prod_choice
        )[0]
