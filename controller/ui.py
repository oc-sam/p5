from typing import List
from utils.controllers import CtrlTools
from utils.ressources.ui.text import INTRO_ART, INTRO_TXT, OUTRO_TXT


class UIController(CtrlTools):
    """
    UIController is the controller class responsible of user interfacing.

    Args:
        CtrlTools (class): General puposes controller tool class.
    """
    def disp_id_name(self, list_of_elts, title: str):
        """
        disp_id_name diplay id and name.

        Args:
            list_of_elts (list): list of element infos
            title (str): type of element
        """
        print(f"Liste des {title}:\n")
        for elt in list_of_elts:
            print(f"{elt[0]}: {elt[1]}")
        self.disp_sep()

    def disp_prod_sub(self, prods: List[tuple], subs: List[tuple]):
        """
        disp_prod_sub display infos about all substituted prods.
        Args:
            prods (List[tuple]): list of substituted prods.
            subs (List[tuple]): list of substitutes.
        """
        print("Liste des produits et de leurs subsituts:\n")
        for prod, sub in zip(prods, subs):
            print(
                "Produit: \n"
                f"  id = {prod[0]}\n"
                f"  nom = {prod[1]}\n"
                "Substitut: \n"
                f"  id = {sub[0]}\n"
                f"  nom = {sub[1]}\n"
                "----"
            )
        self.disp_sep()

    def disp_prod_infos(self, prod_related_data, prod_data, title: str):
        """
        disp_prod_infos display product data.

        Products related to init_categories data.

        Args:
            prod_related_data (dict): info about product brands and stores.
            sub_data (tuple): info about product
            title (str): titre
        """
        for k, v in prod_related_data.items():
            if k == 'brand':
                brands = list(v)
            if k == 'store':
                stores = list(v)
        infos = (
            f"Nom: {prod_data[1]}\n"
            f"Marques: {'; '.join(brands)}\n"
            f"Nutriscore: {prod_data[3]}\n"
            f"Magasins: {'; '.join(stores)}\n"
            f"Lien vers Open Food Facts: {prod_data[2]}\n"
        )
        print(title)
        print(infos)

    def choose(self, input_text: str, permitted: tuple):
        """
        choose ask and verify user input.

        Args:
            input_text (str): text to display for input.
            permitted (tuple): permitted choices for input.

        Returns:
            Type[permitted[0]]: same type as any permitted choice.
        """
        choice = None
        while choice is None:
            val = input(input_text)
            try:
                val = int(val)
            except ValueError:
                pass
            if val in permitted:
                choice = val
                self.disp_sep()
                return choice
            else:
                self.disp_sep()
                print(
                    f"InputError: {choice} est un choix invalide.\n"
                    f"Choix valides: {permitted}"
                    )
                self.disp_sep()

    @staticmethod
    def intro():
        """
        intro print ascii art.
        """
        with open(INTRO_ART, 'r') as f:
            for line in f:
                print(line.rstrip())
        print(INTRO_TXT)

    @staticmethod
    def outro():
        """
        outro when user want to exit app.
        """
        print(OUTRO_TXT)

    @staticmethod
    def fail_sub():
        """
        fail_sub display message than there's no sub for a prod.
        """
        print(
            "Il n'y a aucun substitut adéquat pour ce produit, "
            "dans la base de donnée :("
        )

    @staticmethod
    def disp_sep():
        """
        disp_sep diplay a separator in UI.
        """
        print(
            "_"*10 + "\n"
            )

    @staticmethod
    def disp_warning(nbr_common_cats):
        """
        disp_warning diplay warning regarding qualitative comparaison between
        a prod and it's substitute.

        Args:
            nbr_common_cats (int): number of categories in common
        """
        print(
            f"Note: ce produit a {nbr_common_cats} "
            "categories en commun avec le produit original."
        )
