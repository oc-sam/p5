from utils.ressources.ui.text import ASK, DISP

from controller.data import DataController
from controller.ui import UIController


class Main:
    """
    Main controller which wraps UI and data tasks.
    """
    uictrl = UIController()

    def __init__(self) -> None:
        """
        Process everything on Main instanciation.
        """
        self.exit = 'n'
        self.uictrl.intro()
        self.req_data()
        while self.exit == 'n':
            init_choice = self.uictrl.choose(ASK["init"], (1, 2))
            while init_choice == 1:
                cat_choice = self.ask_cats()
                prod_choice = self.ask_prods(cat_choice)
                sub_data = self.get_disp_sub(prod_choice)
                if sub_data:
                    self.reg_sub(prod_choice, sub_data)
                else:
                    self.uictrl.fail_sub()
                init_choice = 0
            while init_choice == 2:
                all_prods, all_subs = self.dctrl.get_subs_from_prodsub()
                self.uictrl.disp_prod_sub(all_prods, all_subs)
                init_choice = 0
            self.exit = self.uictrl.choose(ASK["exit"], ('y', 'n'))
        else:
            self.uictrl.outro()

    def req_data(self):
        """
        req_data ask user if he/she/it wants to request data.

        Populate database.
        """
        req_choice = self.uictrl.choose(ASK["request"], ('y', 'n'))
        if req_choice == 'y':
            self.dctrl = DataController()
        elif req_choice == 'n':
            self.dctrl = DataController(request_data=False)

    def ask_cats(self):
        """
        ask_cats ask which category the user wants to select.

        Returns:
            int: category id selected by user.
        """
        all_cats = self.dctrl.get_all_cats()
        self.uictrl.disp_id_name(all_cats, "categories")
        valid_cats_ids = self.dctrl.get_res_ids(all_cats)
        return self.uictrl.choose(ASK['cat'], valid_cats_ids)

    def ask_prods(self, cat_choice):
        """
        ask_prods ask which product the user wants to select, given a category.

        Args:
            cat_choice (int): category id chosen by user.

        Returns:
            int: product id selected by user.
        """
        res_prods = self.dctrl.get_prods_from_cat(cat_choice)
        self.uictrl.disp_id_name(res_prods, "products")
        valid_prod_ids = self.dctrl.get_res_ids(res_prods)
        return self.uictrl.choose(ASK['alim'], valid_prod_ids)

    def get_disp_sub(self, prod_choice):
        """
        get_disp_sub get user informations related to a substitute.

        Fetch a substitute, given a product id.
        Display informations about the product.
        Display informations about its substitute.

        Args:
            prod_choice (int): chosen product id.

        Returns:
            tuple: substitute informations.
        """
        sub_data = self.dctrl.get_sub(prod_choice)
        if sub_data is not None:
            sub_id = sub_data[0]
            prod_data = self.dctrl.get_prod(prod_choice)
            prod_related = self.dctrl.get_prod_related(prod_choice)
            self.uictrl.disp_prod_infos(
                prod_related,
                prod_data,
                DISP["prod"]
                )
            self.uictrl.disp_sep()
            sub_related = self.dctrl.get_prod_related(sub_id)
            self.uictrl.disp_prod_infos(
                sub_related,
                sub_data,
                DISP["sub"]
                )
            self.uictrl.disp_sep()
            return sub_data
        else:
            return None

    def reg_sub(self, prod_choice, sub_data):
        """
        reg_sub save a substitute to database.

        Args:
            prod_choice (int): id of product.
            sub_data (tuple): substitute related data.
        """
        self.uictrl.disp_warning(sub_data[-1])
        reg_choice = self.uictrl.choose(ASK['reg_sub'], ('y', 'n'))
        if reg_choice == 'y':
            self.dctrl.add_sub(prod_choice, sub_data[0])
        if reg_choice == 'n':
            pass
