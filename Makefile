PYTHON_INTERPRETER=python3.8
VENV_PATH=.venv

PYTHON_BIN=$(VENV_PATH)/bin/python
PIP=$(VENV_PATH)/bin/pip

FLAKE=$(VENV_PATH)/bin/flake8

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo
	@echo "  install             -- to install this project with virtualenv and Pip"
	@echo ""
	@echo "  clean               -- to clean EVERYTHING (Warning)"
	@echo "  clean-install       -- to clean Python side installation"
	@echo "  clean-pycache       -- to remove all __pycache__"
	@echo ""
	@echo "  tree                -- to show project structure"
	@echo ""
	@echo "  flake               -- to launch Flake8 checking"
	@echo ""
	@echo

clean-pycache:
	find . -type d -name "__pycache__"|xargs rm -Rf
	find . -name "*\.pyc"|xargs rm -f
.PHONY: clean-pycache

clean-install:
	rm -Rf $(VENV_PATH)
.PHONY: clean-install

clean: clean-install clean-pycache
.PHONY: clean

venv:
	virtualenv -p $(PYTHON_INTERPRETER) $(VENV_PATH)
.PHONY: venv

install: venv
	$(PIP) install -r requirements.txt
.PHONY: install

flake:
	$(FLAKE) --show-source --exclude=.venv/
.PHONY: flake

tree:
	tree -I '.git|$(VENV_PATH)|__pycache__' --dirsfirst
.PHONY: tree
