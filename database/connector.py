from typing import List, Union

import mysql.connector as connector
from settings.db import DB_PARAMS
from utils.ressources.db.tables import TABLES


class DbConnector:
    """
    DbConnector is responsible for db manipulation
    """

    def tables_created(self):
        """
        tables_created checks if tables are already created in db.

        Returns:
            boll: Wether or not there are tables.
        """
        res = self.execute("SHOW TABLES")
        return sorted(sum(res, ())) == sorted(TABLES.keys())

    def connect_db(self, verbose=False):
        """
        connect_db connects to db
        
        Args:
            verbose (bool): if errors should be outputted.

        Returns:
            a mysql connector
        """
        try:
            return connector.connect(**DB_PARAMS)
        except connector.Error as err:
            if verbose:
                print("Something went wrong with connection: {}".format(err.msg))
            else:
                pass

    def get_cursor(self, verbose=False):
        """
        get_cursor get a db cursor
        
        Args:
            verbose (bool): if errors should be outputted.

        Returns:
            a mysql cursor
        """
        try:
            return self.connection.cursor()
        except connector.Error as err:
            if verbose:
                print("Something went wrong with cursor: {}".format(err.msg))
            else:
                pass

    def execute(
        self,
        query: str,
        seq_of_params=None,
        many=False,
        verbose=False
    ) -> Union[None, List[tuple]]:
        """
        execute process execution of query

        Args:
            query (str): the query to be executed
            seq_of_params (optional): values passed into query.
            Defaults to None.
            many (bool, optional): if there's many sets of values.
            Defaults to False.
            verbose (bool): if errors should be outputted.

        Returns:
            Union[None, List[tuple]]: response of sql query
        """
        self.connection = self.connect_db()
        self.cursor = self.get_cursor()
        try:
            if not many:
                self.cursor.execute(query)
            elif many:
                self.cursor.executemany(query, seq_of_params)
        except connector.Error as err:
            if verbose:
                print("Something went wrong with execute: {}".format(err.msg))
            else:
                pass
        finally:
            if query.startswith('INSERT'):
                self.connection.commit()
                self.cursor.close()
                self.connection.close()
            elif query.startswith(('SELECT', 'SHOW')):
                try:
                    row = self.cursor.fetchall()
                    return row
                except connector.Error as err:
                    print("Something went wrong when fetching data: {}".format(err.msg))
                finally:
                    self.cursor.close()
                    self.connection.close()
                

    def create_tables(self):
        """
        create_tables create tables in db
        """
        for table_name in TABLES:
            create_query = TABLES[table_name]
            try:
                self.execute(create_query)
            except connector.Error as err:
                if err.errno == connector.errorcode.ER_TABLE_EXISTS_ERROR:
                    print(f"{table_name} already exists.")
                else:
                    print(f"Something went wrong: {err.msg}.")
