from utils.error.errors import Error, SaleTypeError, UrlDeRireError


class Field:
    """
    Base class for model field
    """
    def __set_name__(self, owner, name: str):
        self._name = name

    def __set__(self, instance, value):
        self._value = value
        instance.__dict__[self._name] = value
        instance._all_fields[self._name] = value

    def __get__(self, instance, instance_type=None):
        if self._name in instance.__dict__:
            return instance.__dict__[self._name]
        else:
            print(f"{instance.__name__} has no {self._name} field.")


class CharField(Field):
    """
    CharField class for string type field.

    Args:
        Field (class): base class for a model field
    """
    def __set__(self, instance, value):
        if Error.sale_type(value, str):
            raise SaleTypeError(
                value,
                self._name,
                str.__name__,
                type(value).__name__
            )
        super().__set__(instance, value)


class IntegerField(Field):
    """
    IntegerField class for integer type field.

    Args:
        Field (class): base class for a model field
    """
    def __set__(self, instance, value):
        if Error.sale_type(value, int):
            raise SaleTypeError(
                value,
                self._name,
                int.__name__,
                type(value).__name__
            )
        super().__set__(instance, value)


class UrlField(Field):
    """
    UrlField class for url type field.

    Args:
        Field (class): base class for a model field
    """
    def __set__(self, instance, value):
        if Error.bad_url(value):
            raise UrlDeRireError(value, self._name)
        super().__set__(instance, value)
