from utils.error.errors import WrongFieldError

from models.manager.abstract.manager import BaseManager


class MetaModel(type):
    """
    MetaModel is a metaclass for any model class

    It was designed for the sole purpose of getting
    the manager accessible through objects attribute,
    without raising any error.

    Args:
        type
    """
    _manager = BaseManager

    def _get_manager(cls):
        return cls._manager(a_model=cls)

    @property
    def objects(cls):
        return cls._get_manager()


class AbstractModel(metaclass=MetaModel):
    """
    AbstractModel bass class for any model.

    Args:
        metaclass (class): metaclass for any model class.
        Defaults to MetaModel.
    """
    def __init__(self, **kwargs):
        """
        __init__ instantiate and verify some models attributes.

        Raises:
            WrongFieldError
            AttributeError
        """
        self._all_fields = {}
        self.table_name = self.__class__.__name__
        if kwargs:
            for field_name, value in kwargs.items():
                if field_name not in self.__class__.__dict__:
                    raise WrongFieldError(field_name, self.table_name)
                else:
                    self.__dict__[field_name] = value
                    self._all_fields[field_name] = value
        else:
            raise AttributeError(
                "No field as been defined: you should declare some field."
                )

    def __repr__(self):
        """
        __repr__ overide string representation of Model object.

        Returns:
            str: string representation of object
        """
        if self._all_fields:
            attrs_format = ", ".join([
                f'{field}={value}' for field, value in self._all_fields.items()
                ])
            return f"<{self.table_name}: ({attrs_format})>\n"
        else:
            return super().__repr__()

    @classmethod
    def todb_f_names(cls):
        """
        todb_f_names build a string composed of fields names.

        Returns:
            str: string composed of fields names
        """
        return ', '.join(
            key for key, value in cls.__dict__.items()
            if key != 'id' and
            not key.startswith('_') and
            type(value) is not list
        )

    @classmethod
    def todb_f_count(cls):
        """
        todb_f_count count the number of valid field declared in a model.

        Returns:
            int: number of valid field
        """
        return sum(
            1 for _ in (
                key for key, value in cls.__dict__.items()
                if key != 'id' and
                not key.startswith('_') and
                type(value) is not list
            )
        )

    @property
    def todb_values(self):
        """
        todb_values creat a tuple of fields values

        Returns:
            tuple: tuple of fields values
        """
        return tuple(
            value for value in self._all_fields.values()
            if type(value) is not list
        )
