from models.manager.intermodel import InterModelManager
from .abstract.fields import IntegerField
from .abstract.models import AbstractModel


class ProductSubstitute(AbstractModel):
    """
    ProductSubstitute link related products and substitutes by ids.

    Args:
        AbstractModel (class): bass class for any model
    """
    _manager = InterModelManager

    id_prod = IntegerField()
    id_sub = IntegerField()
