from .abstract.fields import CharField, IntegerField
from .abstract.models import AbstractModel


class Brand(AbstractModel):
    """
    Brand regroup deserialized brands related data.

    Args:
        AbstractModel (class): bass class for any model
    """
    id = IntegerField()
    name = CharField()
