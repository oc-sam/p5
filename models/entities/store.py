from .abstract.fields import CharField, IntegerField
from .abstract.models import AbstractModel


class Store(AbstractModel):
    """
    Store regroup deserialized stores related data.

    Args:
        AbstractModel (class): bass class for any model
    """
    id = IntegerField()
    name = CharField()
