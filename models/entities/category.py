from .abstract.fields import CharField, IntegerField
from .abstract.models import AbstractModel


class Category(AbstractModel):
    """
    Category regroup deserialized categories related data.

    Args:
        AbstractModel (class): bass class for any model
    """
    id = IntegerField()
    name = CharField()
