from models.manager.intermodel import InterModelManager
from .abstract.fields import IntegerField
from .abstract.models import AbstractModel


class ProductBrand(AbstractModel):
    """
    ProductBrand link related products and brands by ids.

    Args:
        AbstractModel (class): bass class for any model
    """
    _manager = InterModelManager

    id_prod = IntegerField()
    id_brand = IntegerField()
