from .abstract.fields import CharField, IntegerField, UrlField
from .abstract.models import AbstractModel


class Product(AbstractModel):
    """
    Product regroup deserialized products related data.

    Args:
        AbstractModel (class): bass class for any model
    """
    id = IntegerField()
    name = CharField()
    url = UrlField()
    nutriscore = CharField()
    brands = []
    stores = []
    categories = []
