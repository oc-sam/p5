from models.manager.intermodel import InterModelManager
from .abstract.fields import IntegerField
from .abstract.models import AbstractModel


class ProductStore(AbstractModel):
    """
    ProductStore link related products and stores by ids.

    Args:
        AbstractModel (class): bass class for any model
    """
    _manager = InterModelManager

    id_prod = IntegerField()
    id_store = IntegerField()
