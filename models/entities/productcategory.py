from models.manager.intermodel import InterModelManager
from .abstract.fields import IntegerField
from .abstract.models import AbstractModel


class ProductCategory(AbstractModel):
    """
    ProductCategory link related products and categories by ids.

    Args:
        AbstractModel (class): bass class for any model
    """
    _manager = InterModelManager

    id_prod = IntegerField()
    id_cat = IntegerField()
