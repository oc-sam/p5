from models.manager.abstract.manager import BaseManager


class InterModelManager(BaseManager):
    """
    InterModelManager overide BaseManager behavior.

    Used it only on iter-models (pair of ids as only model fields)

    Args:
        BaseManager (class): permits db manipulation through model objects
    """
    def __init__(self, a_model) -> None:
        super().__init__(a_model)

    def save(self, data_todb):
        """
        save a model object in its respective sql table.

        Args:
            data_todb (list): data to store in db.
        """
        table_name = self.model.__name__.lower()
        fields_names = self.model.todb_f_names()
        if type(data_todb) is list:
            if all(type(elt) is tuple for elt in data_todb):
                self.bulk_insert(table_name, fields_names, data_todb)
        else:
            self.insert(table_name, fields_names, data_todb)
