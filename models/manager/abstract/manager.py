from typing import List, Tuple, Union

from database.connector import DbConnector
from settings.db import DB_NAME
from utils.error.errors import Error, OperationError


class BaseManager(DbConnector):
    """
    BaseManager permits db manipulation through model objects.

    Args:
        DbConnector (class): responsible for db manipulation
    """
    def __init__(self, a_model) -> None:
        self.model = a_model

    def get(self, wanted: str) -> Union[None, List[tuple]]:
        """
        get all wanted info from a table.

        Args:
            wanted (str): column.s to pick data from.

        Returns:
            Union[None, List[tuple]]: Sql query resulted data
        """
        query = (
                f"SELECT {wanted} FROM {DB_NAME}.{self.model.__name__.lower()};"
            )
        return self.execute(query)

    def filter(
        self,
        wanted: str = None,
        field_name: str = None,
        operation: str = None,
        value: Union[str, int, Tuple[str], Tuple[int]] = None,
        custom_q: str = None,
    ) -> Union[None, List[tuple]]:
        """
        filter on condition an sql table to get only wanted data

        Args:
            wanted (str, optional): column.s to pick data from.
            Defaults to None.
            field_name (str, optional): field.s name.s.
            Defaults to None.
            operation (str, optional): operation used in sql WHERE condition.
            Defaults to None.
            value (Union[str, int, Tuple[str], Tuple[int]], optional):
            value.s used in sql WHERE condition.
            Defaults to None.
            custom_q (str, optional): custom query for custom search.
            Defaults to None.

        Raises:
            OperationError

        Returns:
            Union[None, List[tuple]]: Sql query resulted data
        """
        if not (
            custom_q
            or all(
                elt is None for elt in (
                    wanted,
                    field_name,
                    operation,
                    value
                )
            )
        ):
            if Error.bad_operation(operation):
                raise OperationError(self.filter.__name__, operation)
            if type(value) is tuple:
                if len(value) == 1 and operation == 'IN':
                    value = value[0]
                    operation = '='
            query = (
                f"SELECT {wanted} FROM {DB_NAME}.{self.model.__name__.lower()} "
                f"WHERE {field_name} {operation} {value};"
            )
            return self.execute(query)
        elif custom_q:
            return self.execute(custom_q)

    def insert(self, table_name, fields_names, fields_values):
        """
        insert data into table.

        Args:
            table_name (str): table name.
            fields_names (str): field.s name.s
            fields_values (tuple): field.s value.s.
        """
        query = (
            f"INSERT INTO {DB_NAME}.{table_name} ({fields_names}) "
            f"VALUES {fields_values}"
        )
        self.execute(query)

    def bulk_insert(
        self,
        table_name: str,
        fields_names: Tuple[str],
        fields_values: List[tuple]
    ):
        """
        bulk_insert bulkly insert data in a sql table.

        Args:
            table_name (str): table name.
            fields_names (str): field.s name.s
            fields_values (tuple): field.s value.s.
        """
        values = ", ".join(['%s'] * self.model.todb_f_count())
        query = (
            f"INSERT INTO {DB_NAME}.{table_name} ({fields_names}) "
            f"VALUES ({values})"
        )
        if table_name in ('product', 'category', 'store', 'brand'):
            query += " ON DUPLICATE KEY UPDATE name=name;"
        else:
            query += ";"
        self.execute(query, many=True, seq_of_params=fields_values)

    def get_all(self):
        """
        get_all get all infos

        Returns:
            Union[None, List[tuple]]: Sql query resulted data
        """
        return self.get('*')

    def save(self, data_todb):
        """
        save a model object in its respective sql table.

        Args:
            data_todb (list): data to store in db.
        """
        fields_names = self.model.todb_f_names()
        table_name = self.model.__name__.lower()
        if type(data_todb) is list:
            if all(type(elt) == self.model for elt in data_todb):
                to_save = tuple(instance.todb_values for instance in data_todb)
                self.bulk_insert(table_name, fields_names, to_save)
        else:
            self.insert(table_name, fields_names, data_todb)
