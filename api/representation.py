from typing import List


from utils.representation import LanceRequest
from utils.ressources.api.payload import CATEGORIES, PROD_IN_CAT


class OpenFoodApi(LanceRequest):
    """
    OpenFoodApi purpose is to serve data coming from API.

    Args:
        LanceRequete (class): class tool for OpenFoodApi class
    """
    payload_cat = CATEGORIES
    payload_prod = PROD_IN_CAT

    def get_cat_init(self) -> List[dict]:
        """
        get_cat_init get the most important categories from API

        Returns:
            List[dict]: list of filtered categories
        """
        dirty_categories = self.get_dirty_data(
            self.payload_cat['url'],
            self.payload_cat['params'],
            'tags',
        )
        return self.filtered_data(
            dirty_categories,
            self.payload_cat['wanted_data']
        )

    def get_init_prods(self, categories: List[dict]) -> List[List[dict]]:
        """
        get_init_prods pass products for each initial category into a list

        Args:
            categories (List[dict]): list of filtered categories

        Returns:
            List[List[dict]]: list of list of product for each initial category
        """
        return [
            self.get_prod_related(category['name'])
            for category in categories
            ]

    def get_prod_related(self, category_name: str) -> List[dict]:
        """
        get_prod_related get some products of a category

        Args:
            category_name (str): name of category

        Returns:
            List[dict]: list of products related to one category
        """
        self.payload_prod['params'].update(
            {"tag_0": category_name}
        )
        dirty_products = self.get_dirty_data(
            self.payload_prod['url'],
            self.payload_prod['params'],
            "products",
        )
        return self.cleaned_data(
            ('categories',),
            self.filtered_data(
                dirty_products,
                self.payload_prod['wanted_data']
            )
        )
