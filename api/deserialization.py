from itertools import starmap
from typing import Dict, List, Tuple, Union

from models.entities.brand import Brand
from models.entities.category import Category
from models.entities.product import Product
from models.entities.productcategory import ProductCategory
from models.entities.productstore import ProductStore
from models.entities.store import Store
from utils.deserialization import DesTools
from utils.error.errors import Error

from .representation import OpenFoodApi


class ToInstances(OpenFoodApi):
    """
    ToInstances handles all deserializations.

    Args:
        OpenFoodApi (class): class responsible of representation.
    """
    def __init__(self):
        self.init_instances = self.create_all()

    @staticmethod
    def create_it(
        items_names: List[str],
        itemclass: Union[Category, Brand, Store]
    ):
        """
        create_it instantiate model in a list.

        Args:
            items_names (List[str]): instances names
            itemclass (Union[Category, Brand, Store]): model

        Returns:
            list: model instances.
        """
        return [itemclass(name=item_name) for item_name in items_names]

    def create_prod_cat(list_ids: List[Tuple[int]]) -> List[ProductCategory]:
        """
        create_prod_cat create ProductCategory objects given ids.

        Args:
            list_ids (List[Tuple[int]]): list of tuples of paired ids

        Returns:
            List[ProductCategory]: list of ProductCategory objects
        """
        return [
            ProductCategory(
                id_prod=id_prod,
                id_cat=id_cat
            ) for (id_prod, id_cat) in list_ids
        ]

    def create_prod_sto(list_ids: List[Tuple[int]]) -> List[ProductStore]:
        """
        create_prod_sto create ProductStore objects given ids.

        Args:
            list_ids (List[Tuple[int]]): list of tuples of paired ids

        Returns:
            List[ProductStore]: list of ProductCategory objects
        """
        return [
            ProductStore(
                id_prod=id_prod,
                id_store=id_store
            ) for (id_prod, id_store) in list_ids
        ]

    def create_all(self) -> Dict[str, List[Union[Category, Store, Product]]]:
        """
        create_all creates all needed models objects.

        Returns:
            Dict[str, List[Union[Category, Store, Product]]]:
            a dict composed of list of models instances.
        """
        prod_list, cat_list, store_list, brand_list = [], [], [], []
        for prods in self.get_init_prods(self.get_cat_init()):
            for prod in prods:
                if not Error.missing_values(
                    prod,
                    self.payload_prod['wanted_data']
                ):
                    self.create_it(prod['stores_tags'], Store)
                    store_instances, cat_instances, brand_instances = list(
                        starmap(
                            self.create_it,
                            (
                                (prod['stores_tags'], Store),
                                (prod['categories'], Category),
                                (prod['brands_tags'], Brand)
                            )
                        )
                    )
                    prod_instance = Product(
                            name=prod['generic_name_fr'],
                            url=prod['url'],
                            nutriscore=prod['nutriscore_grade'],
                            brands=brand_instances,
                            stores=store_instances,
                            categories=cat_instances
                    )
                    prod_list.append(prod_instance)
                    cat_list.extend(cat_instances)
                    store_list.extend(store_instances)
                    brand_list.extend(brand_instances)

        cleaned_data = DesTools.clean_duplicates(
            prod_list,
            cat_list,
            store_list,
            brand_list
        )
        return {a_list[0].table_name: a_list for a_list in cleaned_data}
