# Pur Beurre

[![image](https://img.shields.io/github/license/Sam-prog-sudo/sam.github.io?style=flat-square)](https://gitlab.com/oc-sam/p5/-/blob/main/LICENSE)

## Table des matières

- [Contraintes](#contraintes)  
- [Cahier des charges](#cahier-des-charges)
- [Installation](#installation)  
- [Utilisation](#utilisation)  
- [Améliorations possibles](#améliorations-possibles)
- [License](#license)
---
## Contraintes
- Pas de mapper comme sqlalchemy
- Pas de dataclass
- N'utiliser que mysql

---
## Cahier des charges
### Description du parcours utilisateur  

L'utilisateur est sur le terminal. Ce dernier lui affiche les choix suivants :

1. Quel aliment souhaitez-vous remplacer ?
2. Retrouver mes aliments substitués.

L'utilisateur sélectionne 1. Le programme pose les questions suivantes à l'utilisateur et ce dernier sélectionne les réponses :

  - Sélectionnez la catégorie. [Plusieurs propositions associées à un chiffre. L'utilisateur entre le chiffre correspondant et appuie sur entrée]
  - Sélectionnez l'aliment. [Plusieurs propositions associées à un chiffre. L'utilisateur entre le chiffre correspondant à l'aliment choisi et appuie sur entrée]
  - Le programme propose un substitut, sa description, un magasin ou l'acheter (le cas échéant) et un lien vers la page d'Open Food Facts concernant cet aliment.
  - L'utilisateur a alors la possibilité d'enregistrer le résultat dans la base de données.

### Fonctionnalités:

  - Recherche d'aliments dans la base Open Food Facts.
  - L'utilisateur interagit avec le programme dans le terminal, mais si vous souhaitez développer une interface graphique vous pouvez,
  - Si l'utilisateur entre un caractère qui n'est pas un chiffre, le programme doit lui répéter la question,
  - La recherche doit s'effectuer sur une base MySql.

---
## Installation

* Installer [Python 3.8+](https://www.python.org/downloads/).  

* Télécharger le code source.  
Plusieurs solutions s'offrent à vous:  

  - 👯Cloner ce repo sur votre machine local à l'aide de cette adresse:  
https://gitlab.com/oc-sam/p5.git`.  

  - 🍴Forker ce repo.

  - 💾Télécharger le code source à cette adresse:  
https://gitlab.com/oc-sam/p5/-/archive/main/p5-main.zip

* (Optionnel: Utiliser `make install` pour installer les dépendances  et l'environnement virtuel.)

* Installer votre environnement virtuel.  
Exemple avec venv:
```shell
python3 -m venv /chemin_vers_env
```

* Créer une base de donnée mysql.  
* Compléter [les paramètres](https://gitlab.com/oc-sam/p5/-/blob/main/settings/db.py) de la base de donnée, avant de lancer le programme.

----
## Utilisation
* Activer l'environement virtuel.  
```shell
source /chemin_vers_env/bin/activate
```
* Installer les [modules requis](https://gitlab.com/oc-sam/p5/-/blob/main/requirements.txt), si vous n'avez pas utilisé les commandes `make install`. 
```shell
pip install -r requirements.txt
```
* Lancer \_\_main__.py pour démarrer le programme.

----
## Améliorations possibles
Cette liste est non-exhaustive:   

- Rédiger des tests pour la partie API (désrialisation), connecteur base de donnée, les modèles (modèle et champs abstraits) et controlleur.
- Gérer tous les stdout depuis controller.ui .
- Gérer toutes les erreurs avec utils.error.errors.
- Avoir une seule chaine de caractère en argument, lors de l'appel de la méthode model.object.filter().
- Packager les parties connecteur mysql et models.

----
## License
**[MIT license](https://gitlab.com/oc-sam/p5/-/blob/main/LICENSE)**
