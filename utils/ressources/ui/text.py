ASK = {
    "request": (
        "Voulez vous téléchargez les données ?\n"
        "(y/n)\n"
    ),
    "init": (
        "1 - Quel aliment souhaitez-vous remplacer ?\n"
        "2 - Retrouver mes aliments substitués.\n"
        "(1/2)"
    ),
    "cat": (
        "Veuillez sélectionner une catégorie:\n"
        "(Saisissez un nombre correspondant à une catégorie)\n"
    ),
    "alim": (
        "Veuillez sélectionner un aliment:\n"
        "(Saisissez un nombre correspondant à un aliment)\n"
    ),
    "reg_sub": (
        "Voulez vous enregistrer ce substituts pour plus tard ?\n"
        "(y/n)\n"
    ),
    "sub": (
        "Veuillez sélectionner un substitut:\n"
        "(Saisissez un nombre correspondant à un substitut)\n"
    ),
    "exit": (
        "Voulez vous quitter le programme ?\n"
        "(y/n)\n"
    )
}
DISP = {
    "prod": "Produits originaux: ",
    "sub": "Substituts de produits: "
}
INTRO_TXT = "Bonjour !\n"
INTRO_ART = "utils/ressources/ui/art.txt"
OUTRO_TXT = "Au revoir.\n"
