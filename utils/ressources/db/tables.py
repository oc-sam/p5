TABLES = {}

TABLES['product'] = (
    "CREATE TABLE IF NOT EXISTS `product` ("
    "  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `name` varchar(255) NOT NULL,"
    "  `url` text NOT NULL,"
    "  `nutriscore` varchar(1) NOT NULL,"
    "  PRIMARY KEY (`id`),"
    "  UNIQUE (`name`)"
    ") ENGINE=InnoDB"
)

TABLES['category'] = (
    "CREATE TABLE IF NOT EXISTS `category` ("
    "  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `name` varchar(255) NOT NULL,"
    "  PRIMARY KEY (`id`),"
    "  UNIQUE (`name`)"
    ") ENGINE=InnoDB"
)

TABLES['brand'] = (
    "CREATE TABLE IF NOT EXISTS `brand` ("
    "  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `name` varchar(255) NOT NULL,"
    "  PRIMARY KEY (`id`),"
    "  UNIQUE (`name`)"
    ") ENGINE=InnoDB"
)

TABLES['store'] = (
    "CREATE TABLE IF NOT EXISTS `store` ("
    "  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `name` varchar(255) NOT NULL,"
    "  PRIMARY KEY (`id`),"
    "  UNIQUE (`name`)"
    ") ENGINE=InnoDB"
)

TABLES['productcategory'] = (
    "CREATE TABLE IF NOT EXISTS `productcategory` ("
    "  `id_prod` int UNSIGNED NOT NULL,"
    "  `id_cat` int UNSIGNED NOT NULL,"
    "  PRIMARY KEY (`id_prod`, `id_cat`),"
    "  CONSTRAINT `prod_cat_fk_1` FOREIGN KEY (`id_prod`) "
    "     REFERENCES `product` (`id`) ON DELETE CASCADE,"
    "  CONSTRAINT `prod_cat_fk_2` FOREIGN KEY (`id_cat`) "
    "     REFERENCES `category` (`id`) ON DELETE CASCADE"
    ") ENGINE=InnoDB"
)

TABLES['productstore'] = (
    "CREATE TABLE IF NOT EXISTS `productstore` ("
    "  `id_prod` int UNSIGNED NOT NULL,"
    "  `id_store` int UNSIGNED NOT NULL,"
    "  PRIMARY KEY (`id_prod`, `id_store`),"
    "  CONSTRAINT `prod_store_fk_1` FOREIGN KEY (`id_prod`) "
    "     REFERENCES `product` (`id`) ON DELETE CASCADE,"
    "  CONSTRAINT `prod_store_fk_2` FOREIGN KEY (`id_store`) "
    "     REFERENCES `store` (`id`) ON DELETE CASCADE"
    ") ENGINE=InnoDB"
)

TABLES['productsubstitute'] = (
    "CREATE TABLE IF NOT EXISTS `productsubstitute` ("
    "  `id_prod` int UNSIGNED NOT NULL,"
    "  `id_sub` int UNSIGNED NOT NULL,"
    "  PRIMARY KEY (`id_prod`, `id_sub`),"
    "  CONSTRAINT `prod_sub_fk_1` FOREIGN KEY (`id_prod`) "
    "     REFERENCES `product` (`id`) ON DELETE CASCADE,"
    "  CONSTRAINT `prod_sub_fk_2` FOREIGN KEY (`id_sub`) "
    "     REFERENCES `product` (`id`) ON DELETE CASCADE"
    ") ENGINE=InnoDB"
)

TABLES['productbrand'] = (
    "CREATE TABLE IF NOT EXISTS `productbrand` ("
    "  `id_prod` int UNSIGNED NOT NULL,"
    "  `id_brand` int UNSIGNED NOT NULL,"
    "  PRIMARY KEY (`id_prod`, `id_brand`),"
    "  CONSTRAINT `prod_brand_fk_1` FOREIGN KEY (`id_prod`) "
    "     REFERENCES `product` (`id`) ON DELETE CASCADE,"
    "  CONSTRAINT `prod_brand_fk_2` FOREIGN KEY (`id_brand`) "
    "     REFERENCES `brand` (`id`) ON DELETE CASCADE"
    ") ENGINE=InnoDB"
)
