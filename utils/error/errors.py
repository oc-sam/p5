import re

from utils.ressources.api.payload import PROD_IN_CAT

from .constants import OPERATIONS, URL_REGEX


class Error(Exception):
    """Base class for other exceptions"""
    @staticmethod
    def sale_type(value, a_type):
        """
        sale_type test if a value is of expected type.

        [extended_summary]

        Args:
            value (any): value to be tested.
            a_type (any): expected type.

        Returns:
            bool: False if value is of the right type.
        """
        return not isinstance(value, a_type)

    @staticmethod
    def bad_url(value):
        """
        bad_url test if an url is valid.

        Args:
            value (str): url to be tested

        Returns:
            bool: False if value is a valid.
        """
        if not re.search(URL_REGEX, value):
            return True
        else:
            return False

    @staticmethod
    def bad_operation(operation):
        """
        bad_operation test if an operation is valid and permitted.

        Args:
            operation (str): operation to be tested

        Returns:
            bool: False if operation is a valid
        """
        if operation in OPERATIONS:
            return False
        else:
            return True

    @staticmethod
    def missing_values(a_dict, key_values):
        """
        missing_values verify requested data for missing values.

        1/ Checks wether or not the key values are present (might be empty)
        2/ Checks if it contain an empty value.
        3/ Checks if it contains an empty list.

        Args:
            a_dict (dict): requested data.

        Returns:
            bool: True if there's a missing value.
        """
        for value in a_dict.values():
            if set(PROD_IN_CAT['wanted_data']).issubset(a_dict):
                if not value:
                    return True
                elif type(value) is list and '' in value:
                    return True
                else:
                    continue
            else:
                return True


class WrongFieldError(Error):
    """
    WrongFieldError raise when a field is not in declared model's fields.

    Args:
        Error (class): Base class for exceptions.
    """
    def __init__(
        self,
        faulty_field,
        attrs,
    ):
        self.message = ' '.join([
            faulty_field,
            'is not a field defined in',
            attrs,
            ' model.'
            ])
        super().__init__(self.message)


class SaleTypeError(Error):
    """
    SaleTypeError raise when a value is not of expected type.

    Args:
        Error (class): Base class for exceptions.
    """
    def __init__(
        self,
        value,
        field_name,
        field_type,
        faulty_type,
    ):
        self.value = value
        self.message = ' '.join([
            field_name,
            "object expected a",
            field_type,
            "value. You provided a",
            faulty_type,
        ])
        super().__init__(self.message)

    def __str__(self):
        return f"{str(self.value)} -> {self.message}"


class UrlDeRireError(Error):
    """
    UrlDeRireError raise when a supposed url is not valid.

    Args:
        Error (class): Base class for exceptions.
    """
    def __init__(self, value, field_name):
        self.value = value
        self.field_name = field_name
        self.message = "object expected a valid url."
        super().__init__()

    def __str__(self) -> str:
        return f"{self.value} -> {self.field_name} {self.message}"


class OperationError(Error):
    """
    OperationError raise when an is not a valid/permitted sql operation.

    Args:
        Error (class): Base class for exceptions.
    """
    def __init__(self, type_of_query, operation):
        self.type_of_query = type_of_query
        self.operation = operation
        self.message = "require a valid operation."
        super().__init__(self.message)

    def __str__(self) -> str:
        return f"{self.operation} -> {self.type_of_query} {self.message}"
