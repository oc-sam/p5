import requests


class LanceRequest:
    """
    LanceRequest is a tool class for representation puposes.
    """

    @staticmethod
    def get_dirty_data(url: str, payload: dict, key_to_access: str) -> dict:
        """
        get_dirty_data get uncleaned data from openfoodfacts API.

        Args:
            url (str): the url address to fetch data from
            payload (dict): infos to submit to server to filter result of query
            key_to_access (str): key to access data inside json response

        Returns:
            dict: a dict of uncleaned data
        """
        return requests.get(url, params=payload).json()[key_to_access]

    @staticmethod
    def filtered_data(dirty_data: list, wanted_data: tuple) -> list:
        """
        filtered_data strip unwanted data from requested data.

        Args:
            dirty_data (list): list of unfiltered data
            wanted_data (tuple): a tuple of wanted data

        Returns:
            list: list of filtered data
        """
        return [
            {
                key: value for key, value in dirty_dict.items()
                if key in wanted_data
            }
            for dirty_dict in dirty_data
        ]

    @staticmethod
    def cleaned_data(data_keys: tuple, datas: list) -> list:
        """
        cleaned_data clean requested data.

        Args:
            data_keys (tuple): key of values to be cleaned
            datas (list): list of data to be cleaned

        Returns:
            list: list of cleaned data
        """
        for key in data_keys:
            for data in datas:
                data[key] = data[key].split(",")
        return datas
