from typing import List, Tuple


class CtrlTools:
    """
    General puposes controller tool class.
    """
    @staticmethod
    def unpack_res(res):
        """
        unpack_res unpack query result in an appropriate/compatible type.

        Args:
            res (Union[list, tuple]): query result to unpack

        Returns:
            tuple: unpacked result
        """
        if type(res) is list:
            return sum(res, ())
        else:
            return res

    @staticmethod
    def get_res_ids(res):
        """
        get_res_ids extract all ids from query result

        Args:
            res: query result to unpack

        Returns:
            tuple: tuple of ids
        """
        return tuple(elt[0] for elt in res)

    @staticmethod
    def get_id_pair(
        given_id: int,
        related_items: Tuple[str],
        saved_items: List[tuple]
    ) -> List[tuple]:
        return [
            (given_id, elt[0])
            for elt_name in related_items
            for elt in saved_items
            if elt[1] == elt_name
        ]

    @staticmethod
    def get_names(a_list: list) -> Tuple[str]:
        """
        get_names get all names of model instances.

        Args:
            a_list (list): list of model instances.

        Returns:
            Tuple[str]: names.
        """
        return tuple(
            elt.name for elt in a_list
        )

    @staticmethod
    def q_saved(a_model, inst_names: Tuple[str]) -> List[tuple]:
        """
        q_saved query for saved objects.

        Args:
            a_model (class): model to query from.
            inst_names (Tuple[str]): names of instances.

        Returns:
            List[tuple]: [description]
        """
        return a_model.objects.filter(
            wanted='id, name',
            field_name='name',
            operation='IN',
            value=inst_names
        )
