from typing import List


class DesTools:
    """
    General purposes tool class for deserialization.
    """
    @staticmethod
    def clean_duplicates(*all_data) -> List[list]:
        """
        clean_duplicates cleans duplicated instances of models.

        Returns:
            List[list]: cleaned instances.
        """
        cleaned_data = []
        for a_list in all_data:
            seen_names = []
            unique_list = []
            for inst in a_list:
                a_name = inst.name.strip(" .").lower()
                if a_name not in seen_names:
                    seen_names.append(a_name)
                    unique_list.append(inst)
            cleaned_data.append(unique_list)
        return cleaned_data
